#!/usr/bin/python3

from sys import argv, stderr
from gpiozero import TrafficLights, DigitalInputDevice
from time import sleep
from bs4 import BeautifulSoup
from requests import Session
from threading import Thread
from logbook import Logger, StreamHandler

# 1. Abstractions
# ============================================================================

# 1.1. Drupal remote status abstraction
# ----------------------------------------------------------------------------
#   (using requests and bs4)

class DrupalRemoteStatus:
    LOGIN_URL = 'https://lebib.org/user'
    MAIN_URL  = 'https://lebib.org/'
    TOGGLE_URL  = 'https://lebib.org/users/tout-justice-nulle-part'

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self._session = self._open_session()
 
    def _open_session(self):
        session = Session()
        response = session.get(self.LOGIN_URL)
        soup = BeautifulSoup(response.text, 'html.parser')
        login_form = soup.find(id='user-login')
        form_build_id = login_form.find(attrs={"name": "form_build_id"})['value']
        login_post_data = {
            'name': self.username,
            'pass': self.password,
            'form_build_id': form_build_id,
            'form_id': 'user_login',
            'op': 'Se connecter',
        }
        session.post(self.LOGIN_URL, data=login_post_data)
        return session

    def _fetch(self):
        response = self._session.get(self.MAIN_URL)
        soup = BeautifulSoup(response.text, 'html.parser')
        state = bool(soup.find(id='bibopen'))
        form = soup.find(id='bibstate-toggle-form')
        return state, form

    def _toggle(self, form):
        data = {
            'form_id': 'bibstate_toggle_form',
            'form_build_id': form.find(attrs={"name": "form_build_id"})['value'],
            'form_token': form.find(attrs={"name": "form_token"})['value'],
            'op': 'Changer',
        }
        self._session.post(self.TOGGLE_URL, data=data)

    def get(self):
        return self._fetch()[0]

    def set(self, state):
        current, form = self._fetch()
        if state != current:
            self._toggle(form)


# 1.2. Traffic light quick abstraction
# ----------------------------------------------------------------------------
#   used to have somewhere to implement blink()

class StatusLight:
    def __init__(self):
        self._lights = TrafficLights(24, 22, 23)
        self.red = self._lights.red
        self.green = self._lights.green

    def init(self):
        lights = self._lights.amber, self._lights.red, self._lights.green
        for _ in range(3):
            for light in lights:
                light.on()
            sleep(0.5)
            for light in lights:
                light.off()
            sleep(0.5)

    def blink(self):
        self._lights.amber.on()
        sleep(.5)
        self._lights.amber.off()
        sleep(.5)

# 2. Main loop, handles events from the switch and triggers state changes
# ============================================================================

def run(remote, light, switch):
    light.init()
    state = remote.get()
    while True:
        if state != switch.value:
            state = switch.value
            light.red.off() if state else light.green.off()
            thread = Thread(target=remote.set, args=(state,))
            thread.start()
            while thread.is_alive():
                light.blink()
            light.green.on() if state else light.red.on()

# 3. Entry point, build objects required for the main loop
# ============================================================================

if __name__ == "__main__":
    # setup logging
    StreamHandler(stderr).push_application()
    # setup required objects
    remote = DrupalRemoteStatus(argv[1], argv[2])
    light = StatusLight()
    switch = DigitalInputDevice(2, pull_up=True)
    # run main loop
    run(remote, light, switch)